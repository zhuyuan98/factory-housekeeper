import BtnComponents from "./Btn.vue";

const btn = {
  install: function(Vue) {
    Vue.component("Btn", BtnComponents);
  }
};

export default btn;
