import Vue from "vue";
import Router from "vue-router";
import NotDefined from "@views/NotDefined";
import $store from "../store";
import toLogin from "@libs/login";
Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [{
            path: "/",
            name: "home",
            meta: {
                title: "首页",
                keepAlive: true,
                auth: false,
            },
            component: () => import("@views/home.vue"),
        },
        {
            path: "/home",
            name: "home",
            meta: {
                title: "首页",
                keepAlive: true,
                auth: false,
            },
            component: () => import("@views/home.vue"),
        },
        {
            path: "/category",
            name: "category",
            meta: {
                title: "分类",
                keepAlive: true,
                auth: false,
            },
            component: () => import("@views/category.vue"),
        },
        {
            path: "/search",
            name: "search",
            meta: {
                title: "搜索",
                keepAlive: true,
                auth: false,
            },
            component: () => import("@views/search.vue"),
        },
        {
            path: "/detail/:id",
            name: "GoodsCon",
            meta: {
                keepAlive: false,
            },
            component: () => import("@views/goodsDetail.vue"),
        },
        {
            path: "/likeGoods",
            name: "likeGoods",
            meta: {
                title: "猜你喜欢",
                keepAlive: false,
            },
            component: () => import("@views/likeGoods.vue"),
        },
        {
            path: "/discount",
            name: "discount",
            meta: {
                title: "每日特惠",
                keepAlive: false,
            },
            component: () => import("@views/Discount.vue"),
        },
        {
            path: "/Cart",
            name: "Cart",
            meta: {
                title: "购物车",
                keepAlive: true,
                auth: false,
                footer: "none",
            },
            component: () => import("@views/Cart.vue"),
        },
        {
            path: "/orderConfirm/:id",
            name: "orderConfirm",
            meta: {
                title: "订单确认",
                keepAlive: false,
                auth: true,
                toTop: false,
            },
            component: () => import("@views/orderConfirm.vue"),
        },
        {
            path: "/userCenter",
            name: "userCenter",
            meta: {
                title: "个人中心",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/monthEndInfo.vue"),
        },
        {
            path: "/authen",
            name: "authen",
            meta: {
                title: "实名认证",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/authen.vue"),
        },
        {
            path: "/coupon",
            name: "coupon",
            meta: {
                title: "优惠券",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/coupon.vue"),
        },
        {
            path: "/changePwd",
            name: "changePwd",
            meta: {
                title: "修改密码",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/changePwd.vue"),
        },
        {
            path: "/setting",
            name: "setting",
            meta: {
                title: "设置",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/setting.vue"),
        },
        {
            path: "/collection",
            name: "collection",
            meta: {
                title: "我的收藏",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/collection.vue"),
        },
        {
            path: "/footPrint",
            name: "footPrint",
            meta: {
                title: "我的足迹",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/footPrint.vue"),
        },
        {
            path: "/integral",
            name: "integral",
            meta: {
                title: "我的积分",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/integral.vue"),
        },
        {
            path: "/address",
            name: "address",
            meta: {
                title: "收货地址",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/address.vue"),
        },
        {
            path: "/order",
            name: "order",
            meta: {
                title: "我的订单",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/order.vue"),
        },
        {
            path: "/orderDetails/:id",
            name: "orderDetails",
            meta: {
                title: "订单详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/orderDetails.vue"),
        },
        {
            path: "/logistics/:id",
            name: "logistics",
            meta: {
                title: "订单详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/Logistics.vue"),
        },
        {
            path: "/orderRefund/:id",
            name: "orderRefund",
            meta: {
                title: "售后",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/orderRefund.vue"),
        },
        {
            path: "/evaluate/:id",
            name: "evaluate",
            meta: {
                title: "评价",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/Evaluate.vue"),
        },
        {
            path: "/pay/:id",
            name: "pay",
            meta: {
                title: "支付",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/pay.vue"),
        },
        {
            path: "/about",
            name: "about",
            meta: {
                title: "关于我们",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/about.vue"),
        },
        {
            path: "/partners",
            name: "partners",
            meta: {
                title: "合作招商",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/partners.vue"),
        },
        {
            path: "/privacy",
            name: "privacy",
            meta: {
                title: "隐私政策",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/privacy.vue"),
        },
        {
            path: "/opinion",
            name: "opinion",
            meta: {
                title: "意见反馈",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/opinion.vue"),
        },
        {
            path: "/help",
            name: "help",
            meta: {
                title: "帮助中心",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/help.vue"),
        },
        {
            path: "/articleDetail",
            name: "articleDetail",
            meta: {
                title: "文章详情",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/articleDetail.vue"),
        },
        {
            path: "/login",
            name: "login",
            meta: {
                title: "登录",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/login/login.vue"),
        },
        {
            path: "/register",
            name: "register",
            meta: {
                title: "注册",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/login/register.vue"),
        },
        {
            path: "/findPassword",
            name: "findPassword",
            meta: {
                title: "找回密码",
                keepAlive: false,
                auth: false,
            },
            component: () => import("@views/login/findPassword.vue"),
        },
        {
            path: "/demandList",
            name: "demandList",
            meta: {
                title: "需求上架",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/demand/index.vue"),
        },
        {
            path: "/demandCreate",
            name: "demandCreate",
            meta: {
                title: "新增需求上架",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/demand/create.vue"),
        },
        {
            path: "/demandDetail/:id",
            name: "demandDetail",
            meta: {
                title: "需求上架详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/demand/detail.vue"),
        },
        {
            path: "/oemIndex",
            name: "oemIndex",
            meta: {
                title: "生产代工",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/oem/index.vue"),
        },
        {
            path: "/oemCreate",
            name: "oemCreate",
            meta: {
                title: "新增生产代工",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/oem/create.vue"),
        },
        {
            path: "/oemDetail/:id",
            name: "oemDetail",
            meta: {
                title: "生产代工详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/oem/detail.vue"),
        },
        {
            path: "/noStandCustomDetail/:id",
            name: "noStandCustomDetail",
            meta: {
                title: "非标定制详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/noStandCustom/detail.vue"),
        },
        {
            path: "/noStandCustomCreate",
            name: "noStandCustomCreate",
            meta: {
                title: "非标定制新增",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/noStandCustom/create.vue"),
        },
        {
            path: "/noStandCustomIndex",
            name: "noStandCustomIndex",
            meta: {
                title: "非标定制",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/noStandCustom/index.vue"),
        },
        {
            path: "/deviceServiceIndex",
            name: "deviceServiceIndex",
            meta: {
                title: "设备维修",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/deviceService/index.vue"),
        },
        {
            path: "/deviceServiceDetail/:id",
            name: "deviceServiceDetail",
            meta: {
                title: "设备维修详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/deviceService/detail.vue"),
        },
        {
            path: "/deviceServiceCreate",
            name: "deviceServiceCreate",
            meta: {
                title: "设备维修新增",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/deviceService/create.vue"),
        },
        {
            path: "/outSourceIndex",
            name: "outSourceIndex",
            meta: {
                title: "运维外包",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/outSource/index.vue"),
        },
        {
            path: "/outSourceCreate",
            name: "outSourceCreate",
            meta: {
                title: "运维外包新增",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/outSource/create.vue"),
        },
        {
            path: "/outSourceDetail/:id",
            name: "outSourceDetail",
            meta: {
                title: "设备维修详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/outSource/detail.vue"),
        },
        {
            path: "/techChangeIndex",
            name: "techChangeIndex",
            meta: {
                title: "技工改造",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/techChange/index.vue"),
        },
        {
            path: "/techChangeDetail/:id",
            name: "techChangeDetail",
            meta: {
                title: "技工改造详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/techChange/detail.vue"),
        },
        {
            path: "/techChangeCreate",
            name: "techChangeCreate",
            meta: {
                title: "技工改造新增",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/techChange/create.vue"),
        },
        {
            path: "/feedbackIndex",
            name: "feedbackIndex",
            meta: {
                title: "意见反馈",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/feedback/index.vue"),
        },
        {
            path: "/feedbackDetail",
            name: "feedbackDetail",
            meta: {
                title: "意见反馈详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/feedback/detail.vue"),
        },
        {
            path: "/feedbackCreate",
            name: "feedbackCreate",
            meta: {
                title: "意见反馈新增",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/feedback/create.vue"),
        },
        {
            path: "/stockList",
            name: "stockList",
            meta: {
                title: "采购清单",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/stock/list.vue"),
        },
        {
            path: "/stockEdit/:id",
            name: "stockEdit",
            meta: {
                title: "编辑清单",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/stock/edit.vue"),
        },
        {
            path: "/stockDetail/:id",
            name: "stockDetail",
            meta: {
                title: "采购清单详情 ",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/stock/detail.vue"),
        },
        {
            path: "/monthEndInfo",
            name: "monthEndInfo",
            meta: {
                title: "月结信息",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/monthEndInfo.vue"),
        },
        {
            path: "/payMonth",
            name: "payMonth",
            meta: {
                title: "支付金额",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/payMonth.vue"),
        },
        {
            path: "/billDetail",
            name: "billDetail",
            meta: {
                title: "账单明细",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billDetail.vue"),
        },
        {
            path: "/openMonth",
            name: "openMonth",
            meta: {
                title: "开通月结",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/openMonth.vue"),
        },
        {
            path: "/approver",
            name: "approver",
            meta: {
                title: "审核人管理",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/approver.vue"),
        },
        {
            path: "/billInfoIndex",
            name: "billInfoIndex",
            meta: {
                title: "开票信息管理",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billInfo/index.vue"),
        },
        {
            path: "/billInfoAdd",
            name: "billInfoAdd",
            meta: {
                title: "新增开票信息",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billInfo/add.vue"),
        },
        {
            path: "/billInfoEdit",
            name: "billInfoEdit",
            meta: {
                title: "编辑开票信息",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billInfo/edit.vue"),
        },
        {
            path: "/billInfoSet",
            name: "billInfoSet",
            meta: {
                title: "开票信息管理",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billInfo/set.vue"),
        },{
            path: "/billInfoList",
            name: "billInfoList",
            meta: {
                title: "开票记录",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billInfo/list.vue"),
        },{
            path: "/billInfoDetail",
            name: "billInfoDetail",
            meta: {
                title: "开票记录详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/billInfo/detail.vue"),
        },{
            path: "/set",
            name: "set",
            meta: {
                title: "设置",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/set.vue"),
        },{
            path: "/aboutUs",
            name: "aboutUs",
            meta: {
                title: "关于我们",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/aboutUs.vue"),
        },{
            path: "/helpCenter",
            name: "helpCenter",
            meta: {
                title: "帮助中心",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/helpCenter.vue"),
        },{
            path: "/helpDetail",
            name: "helpDetail",
            meta: {
                title: "帮助中心详情",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/helpDetail.vue"),
        },{
            path: "/waitPaymentList",
            name: "waitPaymentList",
            meta: {
                title: "代付账单",
                keepAlive: false,
                auth: true,
            },
            component: () => import("@views/userCenter/waitPaymentList.vue"),
        },
        
        {
            path: "*",
            name: "NotDefined",
            meta: {
                title: "页面找不到",
                keepAlive: true,
                home: false,
                backgroundColor: "#F4F6FB",
            },
            component: NotDefined,
        },
    ],
    scrollBehavior(to, from) {
        from.meta.scrollTop = window.scrollY;
        return { x: 0, y: to.meta.scrollTop || 0 };
    },
});

const { back, replace } = router;

router.back = function() {
    this.isBack = true;
    back.call(router);
};
router.replace = function(...args) {
    this.isReplace = true;
    replace.call(router, ...args);
};

router.beforeEach((to, form, next) => {
    const { title, backgroundColor, footer, home, auth } = to.meta;
    let token = localStorage.getItem('token')
    if (auth === true && !token) {
        if (form.name === "Login") return;
        return toLogin(true, to.fullPath);
    }
    document.title = title || process.env.VUE_APP_NAME || "工厂管家";
    //判断是否显示底部导航
    footer === true ? $store.commit("SHOW_FOOTER") : $store.commit("HIDE_FOOTER");

    //控制悬浮按钮是否显示
    home === false ? $store.commit("HIDE_HOME") : $store.commit("SHOW_HOME");

    $store.commit("BACKGROUND_COLOR", backgroundColor || "#F5F5F5");
    if (token && !$store.userInfo) {
        $store.dispatch("USERINFO").then(() => {
            next();
        });
    } else {
        next()
    };
});

export default router;