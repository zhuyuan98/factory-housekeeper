import request from "@utils/request";

/**
 * 用户登录
 * @param data object 用户账号密码
 */
export function login(data) {
    return request.post("/login", data, { login: false });
}

/**
 * 用户手机号登录
 * @param data object 用户手机号 也只能
 */
export function loginMobile(data) {
    return request.post("/login/mobile", data, { login: false });
}

/**
 * 用户发送验证码
 * @param data object 用户手机号
 */
export function registerVerify(data) {
    return request.post("/register/verify", data, { login: false });
}

/**
 * 用户手机号注册
 * @param data object 用户手机号 验证码 密码
 */
export function register(data) {
    return request.post("/register", data, { login: false });
}

/**
 * 用户手机号修改密码
 * @param data object 用户手机号 验证码 密码
 */
export function registerReset(data) {
    return request.post("/register/reset", data, { login: false });
}

/*
 * 领取优惠券列表
 * */
export function getCoupon(q) {
    return request.get("/coupons", q, { login: false });
}

/*
 * 点击领取优惠券
 * */
export function getCouponReceive(id) {
    return request.post("/coupon/receive", { couponId: id }, { login: true });
}

/*
 * 批量领取优惠券
 * */
export function couponReceiveBatch(couponId) {
    return request.post("/coupon/receive/batch", { couponId });
}

/*
 * 我的优惠券
 * */
export function getCouponsUser(type) {
    return request.get("/coupons/user/" + type);
}
//帮助中心
export function artHelp(){
    return request.get("/article/help");
}
/*
 * 个人中心
 * */
export function getUser() {
    return request.get("/user");
}
/**
 * 申请月结信息
 */
export function applyMonth(data){
    return request.post("/mbalance/save",data)
}
/**
 * 获取月结信息
 */
export function readMonth(data){
    return request.get("/mbalance/read",data)
}
/*
 * 用户信息
 * */
export function getUserInfo() {
    return request.get("/userinfo");
}
/**
 * 
 * 获取月结金额
 */
export function getMonthSelf(){
    return request.get("/month/self");
}
/**
 * 
 * 获取订单号
 */
export function getOrderId(data){
    return request.get("/order/getorderid",data)
}
/**
 * 
 * @param {月结支付} data 
 */
export function payMonth(data){
    return request.post("/pay/month",data)
}
/**
 * 
 * @param {发起微信支付} data 
 */
export function payOrder(data){
    return request.post("/order/pay",data)
}
/**
 * 
 * @param {账单明细} data 
 */
export function cashLog(data){
    return request.get("/cash/log",data)
}
/*
 * 税款额度查询
 * */
export function searchQuota(data) {
    return request.post("/address/edit", data);
}

/*
 * 个人中心(功能列表)
 * */
export function getMenuUser() {
    return request.get("/menu/user");
}

/*
 * 地址列表
 * */
export function getAddressList(data) {
    return request.get("/address/list", data || {});
}
/**
 * 审批人列表
 */
export function getApproveList(data) {
    return request.get("/approver/list", data || {});
}
/**
 * 审批人详情
 */
export function getApproveDetail(id) {
    return request.get("/approver/read?id=" + id);
}
/**
 * 审批人新增 编辑
 */
export function approveEdit(data) {
    return request.post("/approver/save", data);
}
/**
 * 审批人删除
 */
export function approveDel(id) {
    return request.post("/approver/del", { id: id });
}
/** 

*开票信息列表
*/
export function getInvoiceList(data) {
  return request.get("/invoice/list",data);
}
/**
 * 开票信息新增和编辑
 */
export function invoiceSave(data){
  return request.post("/invoice/save",data);
}
/**
 * 
 * @param {开票信息详情} id 
 */
export function invoiceDetail(id){
    return request.get("/invoice/read/"+id)
}
/**
 * 
 * @param {开票详情} data 
 */
export function recordInfo(id){
    return request.get("/invoice/record/info/"+id)
}
/** 
 * 开票订单列表
*/
export function invoiceOrderList(data){
    return request.get("/invoice/order_list",data)
}
/**
 * 开票记录列表
 */
export function invoiceRecord(data){
    return request.get("/invoice/record",data)
}
export function invoiceIssue(data){
    return request.post("/invoice/issue",data)
}
/*
 * 删除地址
 * */
export function getAddressRemove(id) {
    return request.post("/address/del", { id: id });
}

/*
 * 设置默认地址
 * */
export function getAddressDefaultSet(id) {
    return request.post("/address/default/set", { id: id });
}

/*
 * 获取默认地址
 * */
export function getAddressDefault() {
    return request.get("/address/default");
}

/*
 * 获取单个地址
 * */
export function getAddress(id) {
    return request.get("/address/detail/" + id);
}

/*
 * 修改 添加地址
 * */
export function postAddress(data) {
    return request.post("/address/edit", data);
}

/*
 * 获取收藏产品
 * */
export function getCollectUser(page, limit) {
    return request.get("/collect/user", { page: page, limit: limit });
}
/*
 * 获取足迹
 * */
export function getFootUser(page, limit) {
    return request.get("/foot/user", { page: page, limit: limit });
}
/*
 * 删除收藏产品
 * */
export function getCollectDel(id, category) {
    return request.post("/collect/del", { id: id, category: category });
}

/*
 * 批量收藏产品
 * */
export function postCollectAll(data) {
    return request.post("/collect/all", data);
}

/*
 * 添加收藏产品
 * */
export function getCollectAdd(id, category) {
    return request.post("collect/add", { id: id, category: category });
}

/*
 * 签到配置
 * */
export function getSignConfig() {
    return request.get("/sign/config");
}

/*
 * 签到里的签到列表
 * */
export function getSignList(page, limit) {
    return request.get("/sign/list", { page: page, limit: limit });
}

/*
 * 签到列表
 * */
export function getSignMonth(page, limit) {
    return request.get("/sign/month", { page: page, limit: limit });
}

/*
 * 签到用户信息
 * */
export function postSignUser(sign) {
    return request.post("/sign/user", sign);
}

/*
 * 签到
 * */
export function postSignIntegral(sign) {
    return request.post("/sign/integral", sign);
}

/*
 * 推广数据
 * */
export function getSpreadInfo() {
    return request.get("/commission");
}

/*
 * 推广人列表
 * */
export function getSpreadUser(screen) {
    return request.post("/spread/people", screen);
}

/*
 * 推广人订单
 * */
export function getSpreadOrder(where) {
    return request.post("/spread/order", where);
}

/*
 * 资金明细（types|0=全部,1=消费,2=充值,3=返佣,4=提现）
 * */
export function getCommissionInfo(q, types) {
    return request.get("/spread/commission/" + types, q);
}

/*
 * 积分记录
 * */
export function getIntegralList(q) {
    return request.get("/integral/list", q);
}

/*
 * 提现银行
 * */
export function getBank() {
    return request.get("/extract/bank");
}

/*
 * 提现申请
 * */
export function postCashInfo(cash) {
    return request.post("/extract/cash", cash);
}

/*
 * 会员中心
 * */
export function getVipInfo() {
    return request.get("/user/level/grade");
}

/*
 * 会员等级任务
 * */
export function getVipTask(id) {
    return request.get("/user/level/task/" + id);
}

/*
 * 资金统计
 * */
export function getBalance() {
    return request.get("/user/balance");
}

/*
 * 活动状态
 * */
export function getActivityStatus() {
    return request.get("/user/activity", {}, { login: false });
}

/*
 * 活动状态
 * */
export function getSpreadImg() {
    return request.get("/spread/banner");
}

/*
 * 用户修改信息
 * */
export function postUserEdit(data) {
    return request.post("/user/edit", data);
}

/*
 * 用户修改信息
 * */
export function getChatRecord(to_uid, data) {
    return request.get("user/service/record/" + to_uid, data);
}
/**
 * 购物车导出
 */
export function exportProduct(data){
    return request.post("/export",data)
}
/**
 * 7个分类列表
 */
export function artIcleList(data){
    return request.get("/article/list",data)
}
/**
 * 7个分类发布
 */
export function artIclePublish(data){
    return request.post("/article/publish",data)
}
/**
 * 7个分类详情
 * nanfeng 2020年10月15日09:26:51
 */
export function articleDetail(data){
    return request.get("/article/read", data)
}

export function uploadFile(data){
    return request.post("/article/upload",data)
}
/**
 * 获取意见反馈类型
 */
export function artFeedback(){
    return request.get("/article/feedback");
}
/*
 * 用户修改信息
 * */
export function serviceList() {
    return request.get("user/service/list");
}

/*
 * 公众号充值
 * */
export function rechargeWechat(data) {
    return request.post("/recharge/wechat", data);
}

/*
 * 退出登录
 * */
export function getLogout() {
    return request.get("/logout");
}

/*
 * 绑定手机号
 * */
export function bindingPhone(data) {
    return request.post("binding", data);
}

/*
 * h5切换公众号登录
 * */
export function switchH5Login() {
    return request.post("switch_h5", { from: "wechat" });
}
/*
 * 获取推广人排行
 * */
export function getRankList(q) {
    return request.get("rank", q);
}
/*
 * 获取佣金排名
 * */
export function getBrokerageRank(q) {
    return request.get("brokerage_rank", q);
}
/**
 * 检测会员等级
 */
export function setDetection() {
    return request.get("user/level/detection");
}
/**
 * 充值金额选择
 */
export function getRechargeApi() {
    return request.get("recharge/index");
}
/**
 * 验证码key
 */
export function getCodeApi() {
    return request.get("verify_code", {}, { login: false });
}

/*
 * 溪风 新增 消息列表
 * */
export function getMyMessage(where) {
    return request.post("/user/my_message", where);
}

/*
 * 溪风 新增 积分规则
 * */
export function getIntegralRule(where) {
    return request.get("/user/point_rule", where);
}

/*
 * 南枫 新增 实名认证提交
 * */
export function checkRealName(where) {
    return request.post("/check_realname_name", where);
}

/*
 * 南枫 新增 实名认证获取
 * */
export function getRealName(where) {
    return request.post("/check_realname_info", where);
}
/*
 * 南枫 新增 实名认证取消
 * */
export function cancelRealName() {
    return request.get("/empty_certification", {});
}

/*
 * 望舒 新增 帮助中心列表
 * */
export function helpHome(where) {
    return request.post("/helpcenter", where, { login: false });
}

/*
 * 望舒 新增 帮助中心详情
 * */
export function helpInfo(id) {
    return request.get("/helpinfo", id, { login: false });
}

/*
 * 望舒 新增 客服中心
 * */
export function customer() {
    return request.post("/customer", {}, { login: false });
}