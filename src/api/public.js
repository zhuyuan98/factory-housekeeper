import request from "@utils/request";

/**
 * 首页
 * @returns {*}
 */
export function getHomeData() {
    return request.get("index", {}, { login: false });
}

/**
 * PC首页
 * @returns {*}
 */
export function getPcHomeData(data) {
    return request.post("index_pc", data, { login: false });
}

/**
 * 文章 轮播列表
 * @returns {*}
 */
export function getArticleBanner() {
    return request.get("/article/banner/list", {}, { login: false });
}
/**
 * PC首页 轮播列表
 * @returns {*}
 */
export function getPcBanner() {
    return request.get("pc_banner", {}, { login: false });
}
/** 
 * 底部数据
 */
export function getFooter() {
    return request.get("/article/conf", {}, { login: false })
}
/**
 * 文章分类列表
 * @returns {*}
 */
export function getArticleCategory() {
    return request.get("/article/category/list", {}, { login: false });
}

/**
 * 文章 热门列表
 * @returns {*}
 */
export function getArticleHotList() {
    return request.get("/article/hot/list", {}, { login: false });
}

/**
 * 底部说明文案
 * @returns {*}
 */
export function pcSiteBrief() {
    return request.get("/pc_site_brief", {}, { login: false });
}

/**
 * 底部文章
 * @returns {*}
 */
export function pcSysArticle() {
    return request.get("/pc_sys_article", {}, { login: false });
}

/**
 * 文章列表
 * @returns {*}
 */
export function getArticleList(q, cid) {
    return request.get("/article/list/" + cid, q, { login: false });
}

/**
 * 分享
 * @returns {*}
 */
export function getShare() {
    return request.get("/share", {}, { login: false });
}

/**
 * 文章详情
 * @returns {*}
 */
export function getArticleDetails(id) {
    return request.get("/article/details/" + id, {}, { login: false });
}

/**
 * 获取微信sdk配置
 * @returns {*}
 */
export function getWechatConfig() {
    return request.get(
        "/wechat/config", { url: document.location.href }, { login: false }
    );
}

/**
 * 获取微信sdk配置
 * @returns {*}
 */
export function wechatAuth(code, spread, login_type) {
    return request.get(
        "/wechat/auth", { code, spread, login_type }, { login: false }
    );
}
/**
 * 获取快递公司
 * @returns {*}
 */
export function getLogistics() {
    return request.get("/logistics", {}, { login: false });
}

/**
 * 获取登录logo
 * @returns {*}
 */
export function getLogo(type) {
    return request.get("/wechat/get_logo", { type: type }, { login: false });
}

/**
 * 获取图片base64
 * @retins {*}
 * */
export function imageBase64(image, code) {
    return request.post(
        "/image_base64", { image: image, code: code }, { login: false }
    );
}

/**
 * 获取关注海报
 * @returns {*}
 */
export function follow() {
    return request.get("/wechat/follow", {}, { login: false });
}

/**
 * 获取城市列表
 * @returns {*}
 */
export function getCity() {
    return request.get("/city_list", {}, { login: false });
}

/**
 * 获取七牛云信息
 * @returns {*}
 */
export function qiniuToken() {
    return request.get("/qiniu_token", {});
}

/**
 * 关于我们
 * @returns {*}
 */
export function systemArticle() {
    return request.get("/system_article", {}, { login: false });
}

/**
 * 资质文章
 * @returns {*}
 */
export function platformArticle() {
    return request.get("/platform_article", {}, { login: false });
}

/**
 * 资质文章详情
 * @returns {*}
 */
export function platformArticleInfo(id) {
    return request.get("/platform_article_info", { id: id }, { login: false });
}
export function transforPay(){
    return request.post("/pay/transfer",{})
}
export function cashLog(data){
    return request.get("/cash/log",data)
}
export function monthSelf(data){
    return request.get("/month/self",data)
}