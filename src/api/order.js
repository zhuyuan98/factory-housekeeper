/*
 * 订单确认
 * */
import request from "@utils/request";

/**
 * 通过购物车 id 获取订单信息
 * @param cartId
 * @returns {*}
 */
export function postOrderConfirm(cartId) {
  return request.post("/order/confirm", { cartId });
}

/**
 * 计算订单金额
 * @param key
 * @param data
 * @returns {*}
 */
export function postOrderComputed(key, data) {
  return request.post("/order/computed/" + key, data);
}

/**
 * 获取指定金额可用优惠券
 * @param price
 * @returns {*}
 */
export function getOrderCoupon(price, data) {
  return request.get("/coupons/order/" + (parseFloat(price) || 0), data);
}

/**
 * 生成订单
 * @param key
 * @param data
 * @returns {*}
 */
export function createOrder(key, data) {
  return request.post("/order/create/" + key, data || {});
}

/**
 * 订单统计数据
 * @returns {*}
 */
export function getOrderData() {
  return request.get("/order/data");
}
/**
 * 
 * 查询是否有权限使用月结卡 
 */
export function findMonth(data){
  return request.post("/pay/confirm",data)
}
/**
 * 订单列表
 * @returns {*}
 */
export function getOrderList(data) {
  return request.get("/order/list", data);
}
/**
 * 查询物流
 * @param {*} data 
 */
export function expressFind(data){
  return request.get("/order/express",data);
}
/**
 * 订单列表
 * @returns {*}
 */
export function getOrderListPc(data) {
  return request.get("/order/list_pc", data);
  
}

/**
 * 取消订单
 * @returns {*}
 */
export function cancelOrder(id) {
  return request.post("/order/cancel", { id });
}

/**
 * 订单详情
 * @returns {*}
 */
export function orderDetail(id) {
  return request.get("/order/detail/" + id);
}

/**
 * 退款理由
 * @returns {*}
 */
export function getRefundReason() {
  return request.get("/order/refund/reason");
}

/**
 * 提交退款
 * @returns {*}
 */
export function postOrderRefund(data) {
  return request.post("/order/refund/verify", data);
}

/**
 * 确认收货
 * @returns {*}
 */
export function takeOrder(uni) {
  return request.post("/order/take", { uni });
}

/**
 * 删除订单
 * @returns {*}
 */
export function delOrder(uni) {
  return request.post("/order/del", { uni });
}

/**
 * 订单查询物流信息
 * @returns {*}
 */
export function express(uni) {
  return request.get("order/express/" + uni);
}

/**
 * 订单查询物流信息
 * @returns {*}
 */
export function payOrder(uni, paytype, from) {
  return request.post("order/pay", { uni, paytype, from });
}
/**
 * 订单核销
 * @returns {*}
 */
export function orderVerific(verify_code, is_confirm) {
  return request.post("order/order_verific", { verify_code, is_confirm });
}

/**
 * 再次下单
 * @param string uni
 *
 */
export function orderAgain(uni) {
  return request.post("order/again", { uni: uni });
}

/**
 * 再次下单
 * @param string uni
 *
 */
export function orderPayState(uni) {
  return request.get("order/order_pay_state/" + uni);
}

/**
 * nanfeng 2020年10月14日10:29:39
 * 采购清单 
 *
 */
export function purchaseList(data) {
  return request.get("/purchase/list/"+data.type, data);
}

/**
 * nanfeng 2020年10月14日10:29:39
 * 获取清单名称 
 *
 */
export function purchaseOrderName(id) {
  return request.get("/purchase/ordername/"+id);
}

/**
 * nanfeng 2020年10月14日10:29:39
 * 修改订单 
 *
 */
export function updateOrder(data) {
  return request.post("/purchase/update", data);
}

/**
 * nanfeng 2020年10月14日10:29:39
 * 发布 
 *
 */
export function articlePublish(data) {
  return request.post("/article/publish", data);
}

/**
 * nanfeng 2020年10月20日10:30:45
 * 审核订单
 * @params uid: 审批人uid, status: 驳回传2, 
 *
 */
export function approverPass(data) {
  return request.post("/approver/pass", data);
}
