import { qiniuToken } from "@api/public";
export default {
  data() {
    return {
      // el-upload img
      // url: 'http://upload-z2.qiniup.com',
      url: 'http://gongchangguanjia.3todo.com/api/article/upload',
      dialogImageUrl: "",
      dialogVisible: false,
      qiniuDomail: '',
      qiniuData: {},
      fileList: [], // 上传文件列表
      fileType: ["image/png", "image/jpg", "image/jpeg", "image/gif"]
    };
  },
  mounted() {
    this.getQiniuToken();
  },
  methods: {
    getQiniuToken () {
      qiniuToken()
        .then(res => {
          this.qiniuData.token = res.data.uptoken
          this.qiniuDomail = res.data.pc_domain
        })
        .catch(err => {
          this.$dialog.error(err.msg || "获取七牛数据失败");
        });
    },
    // upload img
    handleRemove(file, fileList) {
      this.fileList = fileList;
    },
    handlePictureCardPreview(file) {
      this.dialogImageUrl = file.url;
      this.dialogVisible = true;
    },
    handleAvatarSuccess(res, file, fileList) {
      console.log('fileList', fileList)
      this.fileList = fileList;
    },
    beforeAvatarUpload(file) {
      const isImage = this.fileType.indexOf(file.type) > -1;
      const isLt20M = file.size / 1024 / 1024 < 20;

      // if (!isImage) {
      //   this.$dialog.error('请上传图片类型文件');
      // }
      if (!isLt20M) {
        this.$dialog.error('请上传小于20M文件！');
      }
      // return isImage && isLt20M;
      return isLt20M;
    },
    // format files
    formatFile (fileList) {
      let imgs = fileList.map((item)=>{
        let res = item.response.data[0]
        return res
      })
      return imgs
    }
  }
};
