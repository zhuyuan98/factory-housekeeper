export function downloadFileBlob(url, filename) {
    console.log(url, filename)
    var ele = document.createElement('a');// 创建下载链接
    ele.download = filename;//设置下载的名称
    ele.style.display = 'none';// 隐藏的可下载链接
    // 字符内容转变成blob地址
    var blob = new Blob([url]);
    ele.href = URL.createObjectURL(blob);
    // 绑定点击时间
    document.body.appendChild(ele);
    ele.click();
    // 然后移除
    document.body.removeChild(ele);
};

/**
 * 用FileSave保存文件
 * @param url
 */
export function downloadUrlFile(url, filename) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.setRequestHeader('Authorization', 'Basic a2VybWl0Omtlcm1pdA==');
    xhr.onload = () => {
      if (xhr.status === 200) {
        // 获取图片blob数据并保存
        downloadFileBlob(xhr.response, filename);
      }
    };
    xhr.send();
  }