export function formatSelectData(arr, key1, key2, df = false) {
  var data = [];
  data = arr.map(item => {
    let obj = [];
    obj.value = item[key1];
    obj.label = item[key2];
    return obj;
  });
  if (df) {
    data = [{ label: "全部", value: 0 }, ...data];
  }
  return data;
}

/**
 *
 * 日期时间格式化
 * @param new Date()
 * @param sortType ascending descending
 * @returns String
 */
export function formatDateTime(date, fmt='yyyy-MM-dd') {
  if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
  };
  for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
          let str = o[k] + '';
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
      }
  }
  return fmt;
};

function padLeftZero(str) {
  return ('00' + str).substr(str.length);
};
