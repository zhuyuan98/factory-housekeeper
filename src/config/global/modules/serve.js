/**
 * @Author nanfeng
 * @CreatTime 2020-03-30 14:18
 * @UpdateTime 2020-5-06 10:00
 * @Des HTTP response code 接口返回状态码
 */
/** request return code */
const Serve = {
  /**
   * 请求成功
   */
  SUCCESS_CODE: 1,
  SUCCESS_CODE_MSG: "请求成功",
  /**
   * 请求失败
   */
  FAIL_CODE: 0,
  FAIL_CODE_MSG: "请求失败",
  /**
   * 请求出现语法错误
   */
  PREDICTABLE_CODE: 400,
  PREDICTABLE_CODE_MSG: "请求出现语法错误",
  /**
   * 未授权
   */
  UNAUTHORIZED_CODE: 403,
  UNAUTHORIZED_CODE_MSG: "未授权",
  /**
   * 未找到
   */
  NOT_FOUND_CODE: 404,
  NOT_FOUND_CODE_MSG: "未找到",
  /**
   * Token已过期
   */
  TOKEN_TIME_OUT_CODE: 405,
  TOKEN_TIME_OUT_CODE_MSG: "Token已过期",
  /**
   * 系统异常
   */
  ERROR_CODE: 500,
  ERROR_CODE_MSG: "系统异常"
};

export default {
  Serve
};
