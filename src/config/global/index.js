import Serve from "./modules/serve";
import Default from "./modules/default";
import Image from "../../assets";
export default {
  ...Serve,
  Default,
  Image
};
